<?php
/**Theme Name	: wallstreet-Pro
 * Theme Core Functions and Codes
*/	
	/**Includes reqired resources here**/
	define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());	
	define('WEBRITI_TEMPLATE_DIR',get_template_directory());
	define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');	
	define('WEBRITI_THEME_OPTIONS_PATH',WEBRITI_TEMPLATE_DIR_URI.'/functions/theme_options');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/default_menu_walker.php'); 
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/webriti_nav_walker.php'); 
	//require( WEBRITI_THEME_FUNCTIONS_PATH . '/commentbox/comment-function.php' ); //for comments
	require_once( WEBRITI_THEME_FUNCTIONS_PATH . '/scripts/scripts.php'); 
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/post-type/custom-post-type.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/meta-box/post-meta.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/taxonomies/taxonomies.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/resize_image/resize_image.php'); 
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/excerpt/excerpt.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/custom-sidebar.php');
	// Sidebar Widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wallstreet-latest-widget.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/breadcrumbs/breadcrumbs.php');

	require( WEBRITI_THEME_FUNCTIONS_PATH . '/pagination/webriti_pagination.php'); 
	
	//Webriti shortcodes
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/shortcodes/shortcodes.php' ); //for shortcodes		
	
	//wp title tag starts here
	function webriti_head( $title, $sep )
	{	global $paged, $page;		
		if ( is_feed() )
			return $title;
		// Add the site name.
		$title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( _e( 'Page', 'wallstreet' ), max( $paged, $page ) );
		return $title;
	}	
	add_filter( 'wp_title', 'webriti_head', 10, 2);
	
	add_action( 'after_setup_theme', 'webriti_setup' ); 	
	function webriti_setup()
	{
		global $content_width;
		if ( ! isset( $content_width ) ) $content_width = 600;//In PX
		
		// Load text domain for translation-ready
		load_theme_textdomain( 'wallstreet', WEBRITI_THEME_FUNCTIONS_PATH . '/lang' );
		
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'wallstreet' ) ); //Navigation
		// theme support 	
		$args = array('default-color' => '000000',);
		add_theme_support( 'custom-background', $args  ); 
		add_theme_support( 'automatic-feed-links');
		
		require_once('theme_setup_data.php');
		// setup admin pannel defual data for index page		
		$wallstreet_pro_options=theme_data_setup();
		
		$current_theme_options = get_option('wallstreet_pro_options'); // get existing option data 		
		if($current_theme_options)
		{ 	$wallstreet_pro_options = array_merge($wallstreet_pro_options, $current_theme_options);
			update_option('wallstreet_pro_options',$wallstreet_pro_options);	// Set existing and new option data			
		}
		else
		{
			add_option('wallstreet_pro_options', $wallstreet_pro_options);
		}
		require( WEBRITI_THEME_FUNCTIONS_PATH . '/theme_options/option_pannel.php' ); // for Option Panel Settings		
	}
	// Read more tag to formatting in blog page 	
	function new_content_more($more)
	{  global $post;
		return '<div class=\"blog-btn-col\"><a href="' . get_permalink() . "#more-{$post->ID}\" class=\"blog-btn\">Read More</a></div>";
	}   
	add_filter( 'the_content_more_link', 'new_content_more' );

/* Added By Harish */
	/*===================================================================================
	 * Add Author Links
	 * =================================================================================*/
	function add_to_author_profile( $contactmethods ) {
		$contactmethods['facebook_profile'] = 'Facebook Profile URL';
		$contactmethods['twitter_profile'] = 'Twitter Profile URL';
		$contactmethods['linkedin_profile'] = 'Linkedin Profile URL';
		$contactmethods['google_profile'] = 'Google Profile URL';
		return $contactmethods;
	}
	add_filter( 'user_contactmethods', 'add_to_author_profile', 10, 1);
	
	function add_gravatar_class($class) {
		$class = str_replace("class='avatar", "class='img-responsive comment-img img-circle", $class);
		return $class;
	}
	
	add_filter('get_avatar','add_gravatar_class');

function content_width() {
		global $content_width;
		if ( is_page_template( 'blog-fullwidth.php' ) ){		
			$content_width = 950;
		}
	}
		add_action( 'template_redirect', 'content_width' );

function mfields_set_default_object_terms( $post_id, $post ) {
    if ( 'publish' == $post->post_status && $post->post_type == 'wallstreet_portfolio' ) {
        $taxonomies = get_object_taxonomies( $post->post_type,'object' );
        foreach ( $taxonomies as $taxonomy ) {
            $terms = wp_get_post_terms( $post_id, $taxonomy->name );
			$myid = get_option('wallstreet_webriti_default_term_id');
			$a=get_term_by('id',$myid,'portfolio_categories');
            if ( empty( $terms )) {
                wp_set_object_terms( $post_id,$a->slug , $taxonomy->name );
            }
        }
    }
}
add_action( 'save_post', 'mfields_set_default_object_terms', 100, 2 );
?>