<?php
/************* Home slider custom post type ************************/
	$current_options = get_option('wallstreet_pro_options');	
	$slug_service = $current_options['service_slug'];
	$slug_portfolio = $current_options['portfolio_slug'];
	
		
function wallstreet_slider_type() {
	register_post_type( 'wallstreet_slider',
		array(
			'labels' => array(
			'name' => 'Featured Slider',
			'add_new' => __('Add New Slide', 'wallstreet'),
			'add_new_item' => __('Add New Slider','wallstreet'),
			'edit_item' => __('Add New Slide','wallstreet'),
			'new_item' => __('New Link','wallstreet'),
			'all_items' => __('All Featured Sliders','wallstreet'),
			'view_item' => __('View Link','wallstreet'),
			'search_items' => __('Search Links','wallstreet'),
			'not_found' =>  __('No Links found','wallstreet'),
			'not_found_in_trash' => __('No Links found in Trash','wallstreet'), 
			),
		'supports' => array('title','thumbnail'),
		'show_in' => true,
		'show_in_nav_menus' => false,		
		'public' => true,
		'menu_position' =>20,
		'public' => true,
		'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/slider.png',
		)
	);
}
add_action( 'init', 'wallstreet_slider_type' );

/************* Home Service custom post type ***********************/	
function wallstreet_service_type()
{	register_post_type( 'wallstreet_service',
		array(
			'labels' => array(
			'name' => 'Featured Service',
			'add_new' => __('Add New service', 'wallstreet'),
			'add_new_item' => __('Add New Service','wallstreet'),
			'edit_item' => __('Add New service','wallstreet'),
			'new_item' => __('New Link','wallstreet'),
			'all_items' => __('All Services','wallstreet'),
			'view_item' => __('View Link','wallstreet'),
			'search_items' => __('Search Links','wallstreet'),
			'not_found' =>  __('No Links found','wallstreet'),
			'not_found_in_trash' => __('No Links found in Trash','wallstreet'), 
			),
		'supports' => array('title','thumbnail'),
		'show_in' => true,
		'show_in_nav_menus' => false,
		'rewrite' => array('slug' => $GLOBALS['slug_service']),
		'public' => true,
		'menu_position' =>20,
		'public' => true,
		'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/service.png',
		)
	);
}
add_action( 'init', 'wallstreet_service_type' );


//************* Home project custom post type ***********************
function wallstreet_portfolio_type()
{	register_post_type( 'wallstreet_portfolio',
		array(
			'labels' => array(
				'name' => 'Portfolio / Project',
				'add_new' => __('Add New Item', 'wallstreet'),
				'add_new_item' => __('Add New Project','wallstreet'),
				'edit_item' => __('Add New Portfolio / project','wallstreet'),
				'new_item' => __('New Link','wallstreet'),
				'all_items' => __('All Portfolio Project','wallstreet'),
				'view_item' => __('View Link','wallstreet'),
				'search_items' => __('Search Links','wallstreet'),
				'not_found' =>  __('No Links found','wallstreet'),
				'not_found_in_trash' => __('No Links found in Trash','wallstreet'), 
			),
			'supports' => array('title','editor','thumbnail','excerpt'),
			'show_in' => true,
			'show_in_nav_menus' => false,
			'rewrite' => array('slug' => $GLOBALS['slug_portfolio']),
			'public' => true,
			'menu_position' =>20,
			'public' => true,
			'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/portfolio.png',
		)
	);
}
add_action( 'init', 'wallstreet_portfolio_type' );
/*Testimonial*/
function wallstreet_tesimonial_type()
{	register_post_type( 'wallstreet_testi',
		array(
			'labels' => array(
				'name' => 'Testimonial',
				'add_new' => __('Add New Testimonial', 'wallstreet'),
				'add_new_item' => __('Add New Testimonial','wallstreet'),
				'edit_item' => __('Add New Testimonial','wallstreet'),
				'new_item' => __('New Link','wallstreet'),
				'all_items' => __('All Testimonials','wallstreet'),
				'view_item' => __('View Link','wallstreet'),
				'search_items' => __('Search Testimonials','wallstreet'),
				'not_found' =>  __('No Links found','wallstreet'),
				'not_found_in_trash' => __('No Links found in Trash','wallstreet'), 
			),
			'supports' => array('title', 'thumbnail', 'comments'),
			'show_in' => true,
			'show_in_nav_menus' => false,
			'public' => true,
			'menu_position' =>20,
			'public' => true,
			'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/testimonial.png',
		)
	);
}
add_action( 'init', 'wallstreet_tesimonial_type' );

function wallstreet_team_type()
{	register_post_type( 'wallstreet_team',
		array(
			'labels' => array(
				'name' => 'Our Team',
				'add_new' => __('Add New Team Member', 'wallstreet'),
                'add_new_item' => __('Add New Member','wallstreet'),
				'edit_item' => __('Add New Member','wallstreet'),
				'new_item' => __('New Link','wallstreet'),
				'all_items' => __('All Team Member','wallstreet'),
				'view_item' => __('View Link','wallstreet'),
				'search_items' => __('Search Links','wallstreet'),
				'not_found' =>  __('No Links found','wallstreet'),
				'not_found_in_trash' => __('No Links found in Trash','wallstreet'), 
				),
			'supports' => array('title', 'thumbnail', 'comments'),
			'show_in' => true,
			'show_in_nav_menus' => false,			
			'public' => true,
			'menu_position' => 20,
			'public' => true,
			'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/team.png',
			)
	);
}
add_action( 'init', 'wallstreet_team_type' );

?>