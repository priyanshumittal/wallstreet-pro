<?php
 if ( function_exists( 'add_image_size' ) ) 
 { 
	/*** home slider ***/
	add_image_size('home_slider',1600,750,true);
	
	//Service Image
	add_image_size('webriti_service_img',200,200,true);
	add_image_size('webriti_other_service_img',125,125,true);
	
	/*** portfolio image resize ***/
	add_image_size('portfolio-home-thumb',263,243,true);
	add_image_size('portfolio-2c-thumb',555,510,true);
	add_image_size('portfolio-3c-thumb',360,330,true);
	add_image_size('portfolio-4c-thumb',263,240,true);
	add_image_size('portfolio_detail',750,600,true);
	
	//Testimonial Image
	add_image_size('webriti_testimonial_img',100,100,true);
	
	/***Home blog ***/
	add_image_size('home_blog',370,240,true);
	add_image_size('webriti_blogleft_img',740,400,true);
	add_image_size('webriti_blogfull_img',1140,500,true);

	//ABout Us F'Img
	add_image_size('webriti_aboutus_img',560,390,true);
	//Team Member Image
	add_image_size('webriti_team_member',150,150,true);
	//sidebar Post Widget
	add_image_size('wall_sidebar_img',70,70,true);
}
// code for home slider post types 
add_filter( 'intermediate_image_sizes', 'wallstreet_image_presets');
function wallstreet_image_presets($sizes){
   $type = get_post_type($_REQUEST['post_id']);	
    foreach($sizes as $key => $value){
		if($type=='wallstreet_slider'  &&  $value != 'home_slider')
		{		unset($sizes[$key]);      }
		else if($type=='wallstreet_service' &&  $value != 'webriti_service_img' &&  $value != 'webriti_other_service_img')
		{		unset($sizes[$key]);      }
		else if($type=='wallstreet_portfolio' && $value != 'portfolio-2c-thumb' && $value != 'portfolio-3c-thumb' && $value != 'portfolio-4c-thumb' && $value != 'portfolio-home-thumb' && $value != 'portfolio_detail')
		{		unset($sizes[$key]);  }
		else if($type=='wallstreet_testimonial'  &&  $value != 'webriti_testimonial_img')
		{		unset($sizes[$key]);      }
		else if($type=='post'  && $value != 'webriti_blogleft_img' && $value != 'webriti_blogfull_img' && $value != 'home_blog' &&  $value != 'wall_sidebar_img' )
		{		unset($sizes[$key]);      }
		else if($type=='page'  &&  $value != 'webriti_aboutus_img')
		{		unset($sizes[$key]);      }
		else if($type=='wallstreet_team'  &&  $value != 'webriti_team_member')
		{		unset($sizes[$key]);      }
    }
    return $sizes;	 
}
?>