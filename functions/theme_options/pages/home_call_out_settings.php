<div class="block ui-tabs-panel deactive" id="option-ui-id-5" >	
	<?php $current_options = get_option('wallstreet_pro_options');
	if(isset($_POST['webriti_settings_save_5']))
	{	
		if($_POST['webriti_settings_save_5'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{	$current_options['call_out_title'] = $_POST['call_out_title'];	
				$current_options['call_out_text']=$_POST['call_out_text'];
				$current_options['call_out_button_text']=sanitize_text_field($_POST['call_out_button_text']);
				$current_options['call_out_button_link']=sanitize_text_field($_POST['call_out_button_link']);
				if($_POST['call_out_button_link_target'])
				{ echo $current_options['call_out_button_link_target']=sanitize_text_field($_POST['call_out_button_link_target']); } 
				else
				{ echo $current_options['call_out_button_link_target']="off"; } 
				
				// Call Out Area Enabled on About us or Service Section
				if($_POST['call_out_area_enabled'])
				{ echo $current_options['call_out_area_enabled']= sanitize_text_field($_POST['call_out_area_enabled']); } 
				else { echo $current_options['call_out_area_enabled']="off"; }
				
				update_option('wallstreet_pro_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_5'] == 2) 
		{	
			$current_options['call_out_area_enabled'] = 'on';
			$current_options['call_out_title']="You needs help to create a powerful & strong business?";
			$current_options['call_out_text']="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eros elit, pretium et adipiscing vel, consectetur adipiscing elit.";
			$current_options['call_out_button_text']="See All Feature";
			$current_options['call_out_button_link']="#";	
			$current_options['call_out_button_link_target']="on";					
			update_option('wallstreet_pro_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_5">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Footer Call Out Area','wallstreet');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_5_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_5_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_5_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('5');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('5')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">
			<h3><?php _e('Enable Call Out Area Section :','wallstreet'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['call_out_area_enabled']=='on') echo "checked='checked'"; ?> id="call_out_area_enabled" name="call_out_area_enabled" > <span class="explain"><?php _e('Enable call out area on about us and service section.','wallstreet'); ?></span>
		</div>
		
		<div class="section">		
			<h3><?php _e('Call Out Title','wallstreet'); ?></h3>
			<input class="webriti_inpute"  type="text" name="call_out_title" id="call_out_title" value="<?php if(isset($current_options['call_out_title'])) { echo $current_options['call_out_title']; } ?>" >
			<span class="explain"><?php _e('Enter the call out title.','wallstreet'); ?></span>
		</div>
		
		<div class="section">		
			<h3><?php _e('Call Out Text','wallstreet'); ?></h3>
			<textarea rows="4" cols="8" id="call_out_text" name="call_out_text"><?php if($current_options['call_out_text']!='') { echo esc_attr($current_options['call_out_text']); } ?></textarea>
			<span class="explain"><?php _e('Enter the call out text.','wallstreet'); ?></span>
		</div>
		<div class="section">	
		<h3><?php _e('Call Out Button Text','wallstreet'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="call_out_button_text" id="call_out_button_text" value="<?php if(isset($current_options['call_out_button_text'])) { echo $current_options['call_out_button_text']; } ?>" >
			<span class="explain"><?php _e('Enter the Call Out Button Text.','wallstreet'); ?></span>
		</div>
		<div class="section">	
		<h3><?php _e('Call Out Button Link','wallstreet'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="call_out_button_link" id="call_out_button_link" placeholder="Enter http://example.com"  value="<?php if(isset($current_options['call_out_button_link'])) { echo $current_options['call_out_button_link']; } ?>" >
			<span class="explain"><?php _e('Enter the Call Out Button Link.','wallstreet'); ?></span>
			<p><input type="checkbox" id="call_out_button_link_target" name="call_out_button_link_target" <?php if($current_options['call_out_button_link_target']=='on') echo "checked='checked'"; ?> > <?php _e('Open link in a new window/tab','busi_prof'); ?></p>
		</div>		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_5" name="webriti_settings_save_5" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('5');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('5')" >
		</div>
	</form>
</div>