<div class="block ui-tabs-panel deactive" id="option-ui-id-7" >	
	<?php $current_options = get_option('wallstreet_pro_options');	
	if(isset($_POST['webriti_settings_save_7']))
	{	
		if($_POST['webriti_settings_save_7'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{	
				//About Us Address Settings
				$current_options['contact_address_icon']=sanitize_text_field($_POST['contact_address_icon']);
				$current_options['contact_address_title']=sanitize_text_field($_POST['contact_address_title']);
				$current_options['contact_address_designation_one']=sanitize_text_field($_POST['contact_address_designation_one']);
				$current_options['contact_address_designation_two']=sanitize_text_field($_POST['contact_address_designation_two']);
				
				//About Us Phone Settings
				$current_options['contact_phone_icon']=sanitize_text_field($_POST['contact_phone_icon']);
				$current_options['contact_phone_title']=sanitize_text_field($_POST['contact_phone_title']);
				$current_options['contact_phone_number_one']=sanitize_text_field($_POST['contact_phone_number_one']);
				$current_options['contact_phone_number_two']=sanitize_text_field($_POST['contact_phone_number_two']);
				
				//About Us Email Settings
				$current_options['contact_email_icon']=sanitize_text_field($_POST['contact_email_icon']);
				$current_options['contact_email_title']=sanitize_text_field($_POST['contact_email_title']);
				$current_options['contact_email_number_one']=sanitize_text_field($_POST['contact_email_number_one']);
				$current_options['contact_email_number_two']=sanitize_text_field($_POST['contact_email_number_two']);
				
				//About Us Form Settings
				$current_options['contact_form_title']=sanitize_text_field($_POST['contact_form_title']);
				$current_options['contact_form_description']=sanitize_text_field($_POST['contact_form_description']);
				
				// Address Section Enable in Contact page
				if($_POST['contact_address_settings'])
				{ echo $current_options['contact_address_settings']= sanitize_text_field($_POST['contact_address_settings']); } 
				else { echo $current_options['contact_address_settings']="off"; }
				
				// Phone Section Enable in Contact page
				if($_POST['contact_phone_settings'])
				{ echo $current_options['contact_phone_settings']= sanitize_text_field($_POST['contact_phone_settings']); } 
				else { echo $current_options['contact_phone_settings']="off"; }
				
				// Email Section Enable in Contact page
				if($_POST['contact_email_settings'])
				{ echo $current_options['contact_email_settings']= sanitize_text_field($_POST['contact_email_settings']); } 
				else { echo $current_options['contact_email_settings']="off"; }
				
				update_option('wallstreet_pro_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_7'] == 2) 
		{	
			//About Us Address Settings
			$current_options['contact_address_settings']= 'on';
			$current_options['contact_address_icon']= 'fa-map-marker';
			$current_options['contact_address_title']= 'Address';
			$current_options['contact_address_designation_one']= 'Hoffman Parkway, P.O. Box 353';
			$current_options['contact_address_designation_two']= 'Mountain View. USA';
			
			//About Us Phone Settings
			$current_options['contact_phone_settings']= 'on';
			$current_options['contact_phone_icon']= 'fa-phone';
			$current_options['contact_phone_title']= 'Phone';
			$current_options['contact_phone_number_one']= 'Local: 1-800-123-hello';
			$current_options['contact_phone_number_two']= 'Mobile: 1-800-123-hello';
			
			//About Us Email Settings
			$current_options['contact_email_settings']= 'on';
			$current_options['contact_email_icon']= 'fa-inbox';
			$current_options['contact_email_title']= 'Email';
			$current_options['contact_email_number_one']= 'info@webriti.com';
			$current_options['contact_email_number_two']= 'www.webriti.com';
			
			//About Us Form Settings
			$current_options['contact_form_title']= 'Drop Us a Line';
			$current_options['contact_form_description']= 'Feel Free to write us a Message';	
			
			update_option('wallstreet_pro_options', $current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_7">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Contact Information','wallstreet');?></h2></td>
				<td style="width:30%;">
					<div class="webriti_settings_loding" id="webriti_loding_7_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_7_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_7_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('7');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('7')" >
				</td>
				</tr>
			</table>	
		</div>	
		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		
		<!---Contact Address Section Settings--->
		
		<div class="section">
			<h3><?php _e('Contact Page Address Section Settings','wallstreet'); ?>  </h3>
		</div>
		<div class="section">
			<h3><?php _e('Enable Contact Page Address Section:','wallstreet'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['contact_address_settings']=='on') echo "checked='checked'"; ?> id="contact_address_settings" name="contact_address_settings" > 
		</div>
		<div class="section">
			<h3><?php _e('Contact Address Icon:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_address_icon" id="contact_address_icon" value="<?php if($current_options['contact_address_icon']!='') { echo esc_attr($current_options['contact_address_icon']); } ?>" >
			<h4><?php _e('Service Icon (Using Font Awesome icons name) like: fa-rub .', 'wallstreet'); ?>
			<label style="margin-left:10px;"><a target="_blank" href="http://fontawesome.io/icons/"> <?php _e('Get your fontawesome icons.','wallstreet') ;?></a></label></h4>
		</div>
		<div class="section">
			<h3><?php _e('Contact Address Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_address_title" id="contact_address_title" value="<?php if($current_options['contact_address_title']!='') { echo esc_attr($current_options['contact_address_title']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Contact Aaddress Designation One:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_address_designation_one" id="contact_address_designation_one" value="<?php if($current_options['contact_address_designation_one']!='') { echo esc_attr($current_options['contact_address_designation_one']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Contact Address Designation Two:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_address_designation_two" id="contact_address_designation_two" value="<?php if($current_options['contact_address_designation_two']!='') { echo esc_attr($current_options['contact_address_designation_two']); } ?>" >
		</div>
		
		<!---Contact Phone Section Settings--->
		
		<div class="section">
			<h3><?php _e('Contact Page Phone Section Settings','wallstreet'); ?>  </h3>
		</div>
		<div class="section">
			<h3><?php _e('Enable Contact Page Phone Section:','wallstreet'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['contact_phone_settings']=='on') echo "checked='checked'"; ?> id="contact_phone_settings" name="contact_phone_settings" > 
		</div>
		<div class="section">
			<h3><?php _e('Contact Phone Icon:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_phone_icon" id="contact_phone_icon" value="<?php if($current_options['contact_phone_icon']!='') { echo esc_attr($current_options['contact_phone_icon']); } ?>" >
			<h4><?php _e('Service Icon (Using Font Awesome icons name) like: fa-rub .', 'wallstreet'); ?>
			<label style="margin-left:10px;"><a target="_blank" href="http://fontawesome.io/icons/"> <?php _e('Get your fontawesome icons.','wallstreet') ;?></a></label></h4>
		</div>
		<div class="section">
			<h3><?php _e('Contact Phone Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_phone_title" id="contact_phone_title" value="<?php if($current_options['contact_phone_title']!='') { echo esc_attr($current_options['contact_phone_title']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Contact Phone Number One:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_phone_number_one" id="contact_phone_number_one" value="<?php if($current_options['contact_phone_number_one']!='') { echo esc_attr($current_options['contact_phone_number_one']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Contact Phone Number Two:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_phone_number_two" id="contact_phone_number_two" value="<?php if($current_options['contact_phone_number_two']!='') { echo esc_attr($current_options['contact_phone_number_two']); } ?>" >
		</div>
		
		<!---Contact Email Section Settings--->
		
		<div class="section">
			<h3><?php _e('Contact Page Email Section Settings','wallstreet'); ?>  </h3>
		</div>
		<div class="section">
			<h3><?php _e('Enable Contact Page Email Section:','wallstreet'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['contact_email_settings']=='on') echo "checked='checked'"; ?> id="contact_email_settings" name="contact_email_settings" > 
		</div>
		<div class="section">
			<h3><?php _e('Contact Email Icon:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_email_icon" id="contact_email_icon" value="<?php if($current_options['contact_email_icon']!='') { echo esc_attr($current_options['contact_email_icon']); } ?>" >
			<h4><?php _e('Service Icon (Using Font Awesome icons name) like: fa-rub .', 'wallstreet'); ?>
			<label style="margin-left:10px;"><a target="_blank" href="http://fontawesome.io/icons/"> <?php _e('Get your fontawesome icons.','wallstreet') ;?></a></label></h4>
		</div>
		<div class="section">
			<h3><?php _e('Contact Email Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_email_title" id="contact_email_title" value="<?php if($current_options['contact_email_title']!='') { echo esc_attr($current_options['contact_email_title']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Contact Email One:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_email_number_one" id="contact_email_number_one" value="<?php if($current_options['contact_email_number_one']!='') { echo esc_attr($current_options['contact_email_number_one']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Contact Email Two:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_email_number_two" id="contact_email_number_two" value="<?php if($current_options['contact_email_number_two']!='') { echo esc_attr($current_options['contact_email_number_two']); } ?>" >
		</div>
		
		<!---Contact Form Title Settings--->
		
		<div class="section">
			<h3><?php _e('Contact Form Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_form_title" id="contact_form_title" value="<?php if($current_options['contact_form_title']!='') { echo esc_attr($current_options['contact_form_title']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Contact Form Description:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_form_description" id="contact_form_description" value="<?php if($current_options['contact_form_description']!='') { echo esc_attr($current_options['contact_form_description']); } ?>" >
		</div>
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_7" name="webriti_settings_save_7" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('7');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('7')" >
		</div>
	</form>
</div>