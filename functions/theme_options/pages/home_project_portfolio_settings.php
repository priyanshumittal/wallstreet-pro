<div class="block ui-tabs-panel deactive" id="option-ui-id-4" >	
	<?php $current_options = get_option('wallstreet_pro_options');
	if(isset($_POST['webriti_settings_save_4']))
	{	
		if($_POST['webriti_settings_save_4'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{	
				$current_options['portfolio_list']= sanitize_text_field($_POST['portfolio_list']);
				$current_options['portfolio_title'] = sanitize_text_field($_POST['portfolio_title']);
				$current_options['portfolio_description']= sanitize_text_field($_POST['portfolio_description']);
				$current_options['related_portfolio_title'] = sanitize_text_field($_POST['related_portfolio_title']);
				$current_options['related_portfolio_description']= sanitize_text_field($_POST['related_portfolio_description']);
				$current_options['portfolio_more_text'] = sanitize_text_field($_POST['portfolio_more_text']);
				$current_options['portfolio_more_link'] = sanitize_text_field($_POST['portfolio_more_link']);
				
				if($_POST['view_all_projects_btn_enabled'])
				{echo $current_options['view_all_projects_btn_enabled']=sanitize_text_field($_POST['view_all_projects_btn_enabled']); } 
				else
				{ echo $current_options['view_all_projects_btn_enabled']="off";}
				
				if($_POST['portfolio_more_lnik_target'])
				{ echo $current_options['portfolio_more_lnik_target']=sanitize_text_field($_POST['portfolio_more_lnik_target']); } 
				else
				{ echo $current_options['portfolio_more_lnik_target']="off"; } 
				
				update_option('wallstreet_pro_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_4'] == 2) 
		{
			$current_options['view_all_projects_btn_enabled'] = 'on';
			$current_options['portfolio_list']= 4;
			$current_options['portfolio_more_text'] = "View All Projects";
			$current_options['portfolio_more_link'] = "";
			$current_options['portfolio_more_lnik_target'] = "off";
			$current_options['portfolio_title'] ='Featured Portfolio Project';
			$current_options['portfolio_description'] ='Most Popular of Our Works';
			$current_options['related_portfolio_title'] ='Related Projects';
			$current_options['related_portfolio_description'] ='We Offer Great Services to our Clients';
			update_option('wallstreet_pro_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_4">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Portfolio ','wallstreet');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_4_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_4_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_4_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('4');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('4')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		
		<div class="section">
			<h3><?php _e('Enable View All Projects Button on Front Page :','wallstreet'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['view_all_projects_btn_enabled']=='on') echo "checked='checked'"; ?> id="view_all_projects_btn_enabled" name="view_all_projects_btn_enabled" > <span class="explain"><?php _e('Enable view all projects button on home page(project section).','wallstreet'); ?></span>
		</div>
		<div class="section">
		<h3><?php _e('Number of Portfolio on  project/portfolio section','wallstreet');?></h3>
			<?php $portfolio_list = $current_options['portfolio_list']; ?>		
			<select name="portfolio_list" class="webriti_inpute" >					
				<option value="4" <?php selected($portfolio_list, '4' ); ?>>4</option>
				<option value="8" <?php selected($portfolio_list, '8' ); ?>>8</option>
				<option value="12" <?php selected($portfolio_list, '12' ); ?>>12</option>
				<option value="16" <?php selected($portfolio_list, '16' ); ?>>16</option>
				<option value="20" <?php selected($portfolio_list, '20' ); ?>>20</option>
				<option value="24" <?php selected($portfolio_list, '24' ); ?>>24</option>
			</select>
			<span class="explain"><?php _e('Select number of potfolios','wallstreet'); ?></span>
		</div>
		<div class="section">		
			<h3><?php _e('Portfolio Title','wallstreet'); ?></h3>
			<input class="webriti_inpute"  type="text" name="portfolio_title" id="portfolio_title" value="<?php echo $current_options['portfolio_title']; ?>" >
			<span class="explain"><?php _e('Enter the Portfolio Title.','wallstreet'); ?></span>
		</div>
		<div class="section">	
		<h3><?php _e('Portfolio Description','wallstreet'); ?></h3>			
			<textarea rows="3" cols="8" id="portfolio_description" name="portfolio_description"><?php if($current_options['portfolio_description']!='') { echo esc_attr($current_options['portfolio_description']); } ?></textarea>
			<span class="explain"><?php _e('Enter the Portfolio Description.','wallstreet'); ?></span>
		</div>
		<div class="section">
			<h3><?php _e('Portfolio ','wallstreet'); ?></h3>
			<input  class="webriti_inpute" type="text" name="portfolio_more_text" id="portfolio_more_text"  value="<?php echo $current_options['portfolio_more_text']; ?>" >
			<span class="explain"><?php _e('Enter the Portfolio more button text.','wallstreet'); ?></span>
		</div>
		<div class="section">
			<h3><?php _e('Portfolio More Link','wallstreet'); ?></h3>
			<input type="checkbox" <?php if($current_options['portfolio_more_lnik_target']=='on') echo "checked='checked'"; ?> id="portfolio_more_lnik_target" name="portfolio_more_lnik_target" > <span class="explain"><?php _e('Open link in a new window/tab.','wallstreet'); ?></span>
			<p>
			<input  class="webriti_inpute" type="text" name="portfolio_more_link" id="portfolio_more_link"  placeholder="Enter http://example.com" value="<?php echo $current_options['portfolio_more_link']; ?>" >
			<span class="explain"><?php _e('Enter the Portfolio more button link.','wallstreet'); ?></span>
			</p>
		</div>
		<div class="section">		
			<h3><?php _e('Related Project Title Settings','wallstreet'); ?></h3>
		</div>
		<div class="section">		
			<h3><?php _e('Related Portfolio Title','wallstreet'); ?></h3>
			<input class="webriti_inpute"  type="text" name="related_portfolio_title" id="related_portfolio_title" value="<?php echo $current_options['related_portfolio_title']; ?>" >
			<span class="explain"><?php _e('Enter the Related Portfolio Title.','wallstreet'); ?></span>
		</div>
		<div class="section">	
		<h3><?php _e('Related Portfolio Description','wallstreet'); ?></h3>			
			<textarea rows="3" cols="8" id="related_portfolio_description" name="related_portfolio_description"><?php if($current_options['related_portfolio_description']!='') { echo esc_attr($current_options['related_portfolio_description']); } ?></textarea>
			<span class="explain"><?php _e('Enter the Related Portfolio Description.','wallstreet'); ?></span>
		</div>
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_4" name="webriti_settings_save_4" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('4');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('4')" >
		</div>
		<div class="webriti_spacer"></div>
	</form>
</div>