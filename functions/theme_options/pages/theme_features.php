<div class="block ui-tabs-panel deactive" id="option-ui-id-24" >	
	<?php $current_options = get_option('wallstreet_pro_options');	
	if(isset($_POST['webriti_settings_save_24']))
	{	
		if($_POST['webriti_settings_save_24'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{	
				//Theme Features Settings
				$current_options['theme_feature_image']= sanitize_text_field($_POST['theme_feature_image']);
				$current_options['theme_feature_title'] = sanitize_text_field($_POST['theme_feature_title']);
				$current_options['theme_first_feature_icon'] = sanitize_text_field($_POST['theme_first_feature_icon']);
				$current_options['theme_first_title'] = sanitize_text_field($_POST['theme_first_title']);
				$current_options['theme_first_description'] = sanitize_text_field($_POST['theme_first_description']);
				$current_options['theme_second_feature_icon'] = sanitize_text_field($_POST['theme_second_feature_icon']);
				$current_options['theme_second_title'] = sanitize_text_field($_POST['theme_second_title']);
				$current_options['theme_second_description'] = sanitize_text_field($_POST['theme_second_description']);
				$current_options['theme_third_feature_icon'] = sanitize_text_field($_POST['theme_third_feature_icon']);
				$current_options['theme_third_title'] = sanitize_text_field($_POST['theme_third_title']);
				$current_options['theme_third_description'] = sanitize_text_field($_POST['theme_third_description']);
				
				// Theme Features Section Enable From Front Page
				if($_POST['theme_feature_enabled'])
				{ echo $current_options['theme_feature_enabled']= sanitize_text_field($_POST['theme_feature_enabled']); } 
				else { echo $current_options['theme_feature_enabled']="off"; }
				
				update_option('wallstreet_pro_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_24'] == 2) 
		{	$feature_theme_image = WEBRITI_TEMPLATE_DIR_URI . "/images/desk-image.png";
			//Theme Features Settings Reset
			$current_options['theme_feature_enabled']= 'on';
			$current_options['theme_feature_image']= $feature_theme_image;
			$current_options['theme_feature_title'] = 'Core Features of Theme';
			$current_options['theme_first_feature_icon'] = 'fa-sliders';
			$current_options['theme_first_title'] = 'Incredibly Flexible';
			$current_options['theme_first_description'] = 'Perspiciatis unde omnis iste natus error sit voluptaem omnis iste.';
			$current_options['theme_second_feature_icon'] = 'fa-paper-plane-o';
			$current_options['theme_second_title'] = 'Fully Cutomizable';
			$current_options['theme_second_description'] = 'Perspiciatis unde omnis iste natus error sit voluptaem omnis iste.';
			$current_options['theme_third_feature_icon'] = 'fa-bolt';
			$current_options['theme_third_title'] = 'Ultimate Shortcodes';
			$current_options['theme_third_description'] = 'Perspiciatis unde omnis iste natus error sit voluptaem omnis iste.';
			update_option('wallstreet_pro_options', $current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_24">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Theme Featured Section Settings','wallstreet');?></h2></td>
				<td style="width:30%;">
					<div class="webriti_settings_loding" id="webriti_loding_24_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_24_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_24_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('24');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('24')" >
				</td>
				</tr>
			</table>	
		</div>	
		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		
		<!---Theme Featured Section Settings--->
		<div class="section">
			<h3><?php _e('Enable Theme Featured Section:','wallstreet'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['theme_feature_enabled']=='on') echo "checked='checked'"; ?> id="theme_feature_enabled" name="theme_feature_enabled" ><?php _e('Enable Theme Featured Section From Front Page', 'wallstreet'); ?> 
		</div>
		<div class="section">
			<h3><?php _e('Theme Featured Section Image','wallstreet'); ?>
				<span class="icons help"><span class="tooltip"><?php  _e('Add Theme Featured Image From Here Suggested Size Is 250X250 px','wallstreet');?></span></span>
			</h3>
			<input class="webriti_inpute" type="text" value="<?php if($current_options['theme_feature_image']!='') { echo esc_attr($current_options['theme_feature_image']); } ?>" id="theme_feature_image" name="theme_feature_image" size="36" class="upload has-file"/>
			<input type="button" id="upload_button" value="Add Image" class="upload_image_button" />
			<?php if($current_options['theme_feature_image']!='') { ?>
			<p><img style="height:60px;width:100px;" src="<?php if($current_options['theme_feature_image']!='') { echo esc_attr($current_options['theme_feature_image']); } ?>" /></p>
			<?php } ?>
		</div>
		<div class="section">
			<h3><?php _e('Theme Feature Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_feature_title" id="theme_feature_title" value="<?php if($current_options['theme_feature_title']!='') { echo esc_attr($current_options['theme_feature_title']); } ?>" >
		</div>
		
		<!---First Feature Settting--->
		<div class="section">
			<h3><?php _e('First Feature Icon:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_first_feature_icon" id="theme_first_feature_icon" value="<?php if($current_options['theme_first_feature_icon']!='') { echo esc_attr($current_options['theme_first_feature_icon']); } ?>" >
			<h4><?php _e('First Feature Icon (Using Font Awesome icons name) like: fa-rub .', 'wallstreet'); ?>
			<label style="margin-left:10px;"><a target="_blank" href="http://fontawesome.io/icons/"> <?php _e('Get your fontawesome icons.','wallstreet') ;?></a></label></h4>
		</div>
		<div class="section">
			<h3><?php _e('First Feature Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_first_title" id="theme_first_title" value="<?php if($current_options['theme_first_title']!='') { echo esc_attr($current_options['theme_first_title']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('First Feature Description:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_first_description" id="theme_first_description" value="<?php if($current_options['theme_first_description']!='') { echo esc_attr($current_options['theme_first_description']); } ?>" >
		</div>
		<!---Second Feature Settting--->
		<div class="section">
			<h3><?php _e('Second Feature Icon:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_second_feature_icon" id="theme_second_feature_icon" value="<?php if($current_options['theme_second_feature_icon']!='') { echo esc_attr($current_options['theme_second_feature_icon']); } ?>" >
			<h4><?php _e('Second Feature Icon (Using Font Awesome icons name) like: fa-rub .', 'wallstreet'); ?>
			<label style="margin-left:10px;"><a target="_blank" href="http://fontawesome.io/icons/"> <?php _e('Get your fontawesome icons.','wallstreet') ;?></a></label></h4>
		</div>
		<div class="section">
			<h3><?php _e('Second Feature Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_second_title" id="theme_second_title" value="<?php if($current_options['theme_second_title']!='') { echo esc_attr($current_options['theme_second_title']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Second Feature Description:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_second_description" id="theme_second_description" value="<?php if($current_options['theme_second_description']!='') { echo esc_attr($current_options['theme_second_description']); } ?>" >
		</div>
		<!---Third Feature Settting--->
		<div class="section">
			<h3><?php _e('Third Feature Icon:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_third_feature_icon" id="theme_third_feature_icon" value="<?php if($current_options['theme_third_feature_icon']!='') { echo esc_attr($current_options['theme_third_feature_icon']); } ?>" >
			<h4><?php _e('Third Feature Icon (Using Font Awesome icons name) like: fa-rub .', 'wallstreet'); ?>
			<label style="margin-left:10px;"><a target="_blank" href="http://fontawesome.io/icons/"> <?php _e('Get your fontawesome icons.','wallstreet') ;?></a></label></h4>
		</div>
		<div class="section">
			<h3><?php _e('Third Feature Title:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_third_title" id="theme_third_title" value="<?php if($current_options['theme_third_title']!='') { echo esc_attr($current_options['theme_third_title']); } ?>" >
		</div>
		<div class="section">
			<h3><?php _e('Third Feature Description:','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="theme_third_description" id="theme_third_description" value="<?php if($current_options['theme_third_description']!='') { echo esc_attr($current_options['theme_third_description']); } ?>" >
		</div>
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_24" name="webriti_settings_save_24" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('24');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('24')" >
		</div>
	</form>
</div>