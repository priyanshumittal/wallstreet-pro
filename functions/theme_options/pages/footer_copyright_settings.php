<div class="block ui-tabs-panel deactive" id="option-ui-id-23" >	
	<?php $current_options = get_option('wallstreet_pro_options');
	if(isset($_POST['webriti_settings_save_23']))
	{	
		if($_POST['webriti_settings_save_23'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{		
				$current_options['footer_copyright']=$_POST['footer_copyright'];
				update_option('wallstreet_pro_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_23'] == 2) 
		{
			$current_options['footer_copyright']='Copyright @ 2014 - WALL STREET. Designed by <a href="http://webriti.com" rel="nofollow" target="_blank">Webriti</a>';		
			update_option('wallstreet_pro_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_23">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Footer Customizations','wallstreet');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_23_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_23_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_23_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('23');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('23')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">		
			<h3><?php _e('Footer Customization text','wallstreet'); ?></h3>
			<textarea rows="4" cols="50" class="webriti_inpute" name="footer_copyright" id="footer_copyright" ><?php if(isset($current_options['footer_copyright'])) 
			{ esc_attr_e($current_options['footer_copyright']); } ?> </textarea>
			<span class="explain"><?php  _e('Enter the Footer Customization text','wallstreet');?></span>
		</div>
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_23" name="webriti_settings_save_23" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('23');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('23')" >
		</div>
	</form>
</div>