<div class="block ui-tabs-panel deactive" id="option-ui-id-3" >	
	<?php $current_options = get_option('wallstreet_pro_options');
	if(isset($_POST['webriti_settings_save_3']))
	{	
		if($_POST['webriti_settings_save_3'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{	
				$current_options['service_list']= sanitize_text_field($_POST['service_list']);
				$current_options['service_title'] = sanitize_text_field($_POST['service_title']);
				$current_options['service_description'] = sanitize_text_field($_POST['service_description']);
				$current_options['other_service_title'] = sanitize_text_field($_POST['other_service_title']);
				$current_options['other_service_description'] = sanitize_text_field($_POST['other_service_description']);
				
				// Other Service Section in Service Template
				if($_POST['other_service_section_enabled'])
				{ echo $current_options['other_service_section_enabled']= sanitize_text_field($_POST['other_service_section_enabled']); } 
				else { echo $current_options['other_service_section_enabled']="off"; }
				
				update_option('wallstreet_pro_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_3'] == 2) 
		{
			// Other Service Section in Service Template
			$current_options['other_service_section_enabled']= 'on';
			$current_options['service_list']= 3;
			$current_options['service_title']='Our Services';
			$current_options['service_description'] ='We Offer Great Services to our Clients';
			$current_options['other_service_title'] = 'Other Services';
			$current_options['other_service_description'] = 'We Offer Great Services to our Clients';
			
			update_option('wallstreet_pro_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_3">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Service Settings','wallstreet');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_3_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_3_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_3_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('3');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('3')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		
		<div class="section">
			<h3><?php _e('Services Section','wallstreet'); ?></h3>
			<hr>
			<h3><?php _e('Number of services on service section','wallstreet');?></h3>
			<?php $service_list = $current_options['service_list']; ?>		
			<select name="service_list" class="webriti_inpute" >					
				<option value="3" <?php selected($service_list, '3' ); ?>>3</option>
				<option value="6" <?php selected($service_list, '6' ); ?>>6</option>
				<option value="9" <?php selected($service_list, '9' ); ?>>9</option>
				<option value="12" <?php selected($service_list, '12' ); ?>>12</option>
				<option value="15" <?php selected($service_list, '15' ); ?>>15</option>
				<option value="18" <?php selected($service_list, '18' ); ?>>18</option>
				<option value="21" <?php selected($service_list, '21' ); ?>>21</option>
				<option value="24" <?php selected($service_list, '24' ); ?>>24</option>
			</select>
			<span class="explain"><?php _e('Select number of services','wallstreet'); ?></span>
		</div>
		<div class="section">
			<h3><?php _e('Services Title','wallstreet'); ?></h3>
			<input class="webriti_inpute"  type="text" name="service_title" id="service_title" value="<?php if($current_options['service_title']!='') { echo $current_options['service_title']; } ?>" >
			<span class="explain"><?php _e('Enter the service title.','wallstreet'); ?></span>
		</div>
		<div class="section">	
		<h3><?php _e('Services Description','wallstreet'); ?></h3>
			<textarea rows="3" cols="8" id="service_description" name="service_description"><?php if($current_options['service_description']!='') { echo esc_attr($current_options['service_description']); } ?></textarea>
			<span class="explain"><?php _e('Enter the service description.','wallstreet'); ?></span>
		</div>
		
		<div class="section">
			<h3><?php _e('Other Services Section','wallstreet'); ?></h3>
			<hr>
			<h3><?php _e('Enable Other Service Section in Service Template:','wallstreet'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['other_service_section_enabled']=='on') echo "checked='checked'"; ?> id="other_service_section_enabled" name="other_service_section_enabled" > 
		</div>
		
		<div class="section">
			<h3><?php _e('Other Services Title','wallstreet'); ?></h3>
			<input class="webriti_inpute"  type="text" name="other_service_title" id="other_service_title" value="<?php if($current_options['other_service_title']!='') { echo $current_options['other_service_title']; } ?>" >
			<span class="explain"><?php _e('Enter the service title.','wallstreet'); ?></span>
		</div>
		<div class="section">	
		<h3><?php _e('Other Services Description','wallstreet'); ?></h3>
			<textarea rows="3" cols="8" id="other_service_description" name="other_service_description"><?php if($current_options['other_service_description']!='') { echo esc_attr($current_options['other_service_description']); } ?></textarea>
			<span class="explain"><?php _e('Enter the service description.','wallstreet'); ?></span>
		</div>
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_3" name="webriti_settings_save_3" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('3');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('3')" >
		</div>
	</form>
</div>