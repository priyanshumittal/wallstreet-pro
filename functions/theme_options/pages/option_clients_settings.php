<?php 
$current_options = get_option('wallstreet_pro_options');
if($current_options['total_client']){
$total_client = $current_options['total_client'];
} else { $total_client = 0; }
if(isset($_POST['webriti_settings_save_6']))
{	
	if($_POST['webriti_settings_save_6'] == 1) 
	{
			$total_client = $_POST['total_client'];
			$client_list=array();
			for($i=1; $i<= $total_client; $i++)
			{	$client_image ='client_image_'.$i;
				$client_title ='client_title_'.$i;
				$client_url ='client_url_'.$i;									
				$client_title = $_POST[$client_title];					
				$client_url = $_POST[$client_url];	
				$client_image = $_POST[$client_image];					
				$client_list[$i]=array('client_title'=> $client_title,'client_url'=> $client_url, 'client_image_url'=>$client_image);
				
			}
			$current_options['client_list']= $client_list;
			$current_options['total_client']= $_POST['total_client'];
			$current_options['home_client_title']= sanitize_text_field($_POST['home_client_title']);
			$current_options['home_client_description']=sanitize_text_field($_POST['home_client_description']);
			
			update_option('wallstreet_pro_options', stripslashes_deep($current_options));
	}
	if($_POST['webriti_settings_save_6'] == 2) 
	{	
		$current_options['home_client_title']= 'Our Clients';
		$current_options['home_client_description']='HAVE A LOOK AT OUR CLIENT WE ARE GROWING THEIR BUSINESS AND THEY ARE GOING UP IN THE MARKET BY BEATING THEIR COMPETITORS.';
		$current_options['client_list']='';
		$current_options['total_client']='';			
		update_option('wallstreet_pro_options',$current_options);
	}
} // end of submit client list  

?>
<script type="text/javascript">
	function addInput() 
	{	
	  var client =jQuery('#total_client').val();
	  client++;
	  jQuery('#webriti_client').append('<div id="client-client-'+client+'" class="section" ><h3>Client Title '+client+' </h3><input class="webriti_inpute" type="text" value="" id="client_title_'+client+'" name="client_title_'+client+'" size="36"/><h3>Client website URL '+client+' </h3><input class="webriti_inpute" type="text" value="" id="client_url_'+client+'" name="client_url_'+client+'" size="36"/><h3>Client Image '+client+' </h3><input class="webriti_inpute" type="text" value="" id="client_image_'+client+'" name="client_image_'+client+'" size="36"/><input type="button" id="upload_image_button_'+client+'" value="Client Logo" class="upload_image_button_'+client+'" onClick="webriti_client('+client+')" /></div>');
	  jQuery("#remove_button").show();
	  jQuery('#total_client').val(client);
	}

	function remove_field()
	{
		var client =jQuery('#total_client').val();
		if(client){
		jQuery("#client-client-"+client).remove();
		client=client-1;
		jQuery('#total_client').val(client);
		} 
	
		
	}
	
	function webriti_client(client_id)
	{
		// media upload js
		var uploadID = ''; /*setup the var*/
		//jQuery('.upload_image_button').click(function() {
		var upload_image_button="#upload_image_button_"+client_id;
			uploadID = jQuery(upload_image_button).prev('input'); /*grab the specific input*/			
			formfield = jQuery('.upload').attr('name');
			tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
			
			window.send_to_editor = function(html)
			{
				imgurl = jQuery('img',html).attr('src');
				uploadID.val(imgurl); /*assign the value to the input*/
				tb_remove();
			};		
			return false;
		//});
	}
</script>
<div class="block ui-tabs-panel deactive" id="option-ui-id-6" >
	<form method="post" id="webriti_theme_options_6">
		<div id="heading">			
			<table style="width:100%;">
				<tr><td><h2><?php _e('Client','wallstreet');?></h2></td>
					<td style="width:30%;">
						<div class="webriti_settings_loding" id="webriti_loding_6_image"></div>
						<div class="webriti_settings_massage" id="webriti_settings_save_6_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
						<div class="webriti_settings_massage" id="webriti_settings_save_6_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
					</td>
					<td style="text-align:right;">
						<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('6');">
						<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('6')" >
					</td>
				</tr>				
			</table>	
		</div>
		<div id="webriti_client">
		<?php if($current_options['client_list'])
		{
			$i=1;
			foreach($current_options['client_list'] as $client_list)
			{	?>
				<div class="section" id="client-client-<?php echo $i ?>"> 
					<h3><?php _e('Client Title','wallstreet'); ?> <?php echo $i ?></h3>
					<input type="text" value="<?php echo $client_list['client_title']; ?>" id="client_title_<?php echo $i ?>" name="client_title_<?php echo $i ?>" class="webriti_inpute">
					<h3><?php _e('Client URL','wallstreet'); ?><?php echo $i ?></h3>
					<input type="text" value="<?php echo $client_list['client_url']; ?>" id="client_url_<?php echo $i ?>" name="client_url_<?php echo $i ?>" class="webriti_inpute">
					<h3><?php _e('Client Image','wallstreet');?> <?php echo $i ?> </h3>
					<input type="text" value="<?php echo $client_list['client_image_url']; ?>" id="client_image_<?php echo $i ?>" name="client_image_<?php echo $i ?>" class="webriti_inpute">
					<input type="button" id="upload_button" value="Client Image" class="upload_image_button"  />			<BR>
					<img src="<?php echo $client_list['client_image_url']; ?>" style="height:150px; width:250px;">
				</div>	
			<?php	$i=$i+1;
			}	
		} ?>
		</div>
		<div class="section" style="margin-bottom:30px;">
			<a onclick="addInput()" href="#" class="btn btn-primary" name="add" id="more_faq" ><?php _e('Add Client','wallstreet');?></a>
			<a onclick="remove_field()" href="#" class="btn btn-inverse"  id="remove_button" style="display:<?php if(!$total_client) { ?>none<?php } ?>;"><?php _e('Remove Last Client','wallstreet'); ?></a>		
		</div>
		<div class="section">
			<h3><?php _e('Home Page client Heading','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="home_client_title" id="home_client_title" value="<?php if($current_options['home_client_title']!='') { echo esc_attr($current_options['home_client_title']); } ?>" >		
			<span class="explain"><?php  _e('Enter Heading for client Section.','wallstreet');?></span>
			<h3><?php _e('Home Page Client section description','wallstreet');?></h3>
			<input class="webriti_inpute"  type="text" name="home_client_description" id="home_client_description" value="<?php if($current_options['home_client_description']!='') { echo esc_attr($current_options['home_client_description']); } ?>" >		
			<span class="explain"><?php  _e('Enter Heading for client Section.','wallstreet');?></span>
			</div>
		<input type="hidden" class="webriti_inpute" type="text" id="total_client" name="total_client" value="<?php echo $total_client; ?> " />
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_6" name="webriti_settings_save_6" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('6');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('6')" >
		</div>		
	</form>	
</div>