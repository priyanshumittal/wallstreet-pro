<div class="block ui-tabs-panel deactive" id="option-ui-id-21" >	
	<?php $current_options = get_option('wallstreet_pro_options');
	if(isset($_POST['webriti_settings_save_21']))
	{	
		if($_POST['webriti_settings_save_21'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{		
				$current_options['testi_slide_type']=sanitize_text_field($_POST['testi_slide_type']);
				$current_options['testi_scroll_items']=sanitize_text_field($_POST['testi_scroll_items']);
				$current_options['testi_scroll_dura']=sanitize_text_field($_POST['testi_scroll_dura']);
				$current_options['testi_timeout_dura']=sanitize_text_field($_POST['testi_timeout_dura']);
				
				update_option('wallstreet_pro_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_21'] == 2) 
		{	
			$current_options['testi_slide_type']='scroll';
			$current_options['testi_scroll_items']='1';
			$current_options['testi_scroll_dura']='2000';	
			$current_options['testi_timeout_dura']='1500';
			
			update_option('wallstreet_pro_options', $current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_21">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Testimonial Settings','wallstreet');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_21_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_21_success" ><?php _e('Options data successfully Saved','wallstreet');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_21_reset" ><?php _e('Options data successfully reset','wallstreet');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('21');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('21')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">
			<h3><?php _e('Slide Type Variations','wallstreet'); ?></h3>
			<?php $testi_slide_type = $current_options['testi_slide_type']; ?>		
				<select name="testi_slide_type" class="webriti_inpute" >					
					<option value="scroll"  <?php echo selected($testi_slide_type, 'scroll' ); ?>><?php _e('Scroll','wallstreet');?></option>
					<option value="fade" <?php echo selected($testi_slide_type, 'fade' ); ?>><?php _e('Fade','wallstreet');?></option> 
					<option value="crossfade"  <?php echo selected($testi_slide_type, 'crossfade' ); ?>><?php _e('Cross fade','wallstreet');?></option>
					<option value="cover-fade" <?php echo selected($testi_slide_type, 'cover-fade' ); ?>><?php _e('Cover Fade','wallstreet');?></option> 
				</select>
				<span class="explain"><?php _e('Select Slide Type Variations.','wallstreet'); ?></span>	
		</div>
		<div class="section">
			<h3><?php _e('Scroll Items','wallstreet') ?></h3>
			<?php $testi_scroll_items = $current_options['testi_scroll_items']; ?>		
				<select name="testi_scroll_items" class="webriti_inpute" >					
					<option value="1" <?php selected($testi_scroll_items, '1' ); ?>>1</option>
					<option value="2" <?php selected($testi_scroll_items, '2' ); ?>>2</option>
					<option value="3" <?php selected($testi_scroll_items, '3' ); ?>>3</option>					
				</select>
			<span class="explain"><?php _e('Select items to scroll','wallstreet'); ?></span>	
		</div>
		<div class="section">
			<h3><?php _e('Scroll Duration','wallstreet') ?></h3>
			<?php $testi_scroll_dura = $current_options['testi_scroll_dura']; ?>		
				<select name="testi_scroll_dura" class="webriti_inpute" >					
					<option value="500" <?php selected($testi_scroll_dura, '500' ); ?>>0.5</option>
					<option value="1000" <?php selected($testi_scroll_dura, '1000' ); ?>>1.0</option>
					<option value="1500" <?php selected($testi_scroll_dura, '1500' ); ?>>1.5</option>
					<option value="2000" <?php selected($testi_scroll_dura, '2000' ); ?>>2.0</option>
					<option value="2500" <?php selected($testi_scroll_dura, '2500' ); ?>>2.5</option>
					<option value="3000" <?php selected($testi_scroll_dura, '3000' ); ?>>3.0</option>
					<option value="3500" <?php selected($testi_scroll_dura, '3500' ); ?>>3.5</option>
					<option value="4000" <?php selected($testi_scroll_dura, '4000' ); ?>>4.0</option>
					<option value="4500" <?php selected($testi_scroll_dura, '4500' ); ?>>4.5</option>
					<option value="5000" <?php selected($testi_scroll_dura, '5000' ); ?>>5.0</option>
					<option value="5500" <?php selected($testi_scroll_dura, '5500' ); ?>>5.5</option>
				</select>
				<span class="explain"><?php _e('Select scroll duration.','wallstreet'); ?></span>	
		</div>
		<div class="section">
			<h3><?php _e('Time out Duration','wallstreet') ?></h3>
			<?php $testi_timeout_dura = $current_options['testi_timeout_dura']; ?>		
				<select name="testi_timeout_dura" class="webriti_inpute" >					
					<option value="500" <?php selected($testi_timeout_dura, '500' ); ?>>0.5</option>
					<option value="1000" <?php selected($testi_timeout_dura, '1000' ); ?>>1.0</option>
					<option value="1500" <?php selected($testi_timeout_dura, '1500' ); ?>>1.5</option>
					<option value="2000" <?php selected($testi_timeout_dura, '2000' ); ?>>2.0</option>
					<option value="2500" <?php selected($testi_timeout_dura, '2500' ); ?>>2.5</option>
					<option value="3000" <?php selected($testi_timeout_dura, '3000' ); ?>>3.0</option>
					<option value="3500" <?php selected($testi_timeout_dura, '3500' ); ?>>3.5</option>
					<option value="4000" <?php selected($testi_timeout_dura, '4000' ); ?>>4.0</option>
					<option value="4500" <?php selected($testi_timeout_dura, '4500' ); ?>>4.5</option>
					<option value="5000" <?php selected($testi_timeout_dura, '5000' ); ?>>5.0</option>
					<option value="5500" <?php selected($testi_timeout_dura, '5500' ); ?>>5.5</option>
				</select>
				<span class="explain"><?php _e('Select time out duration.','wallstreet'); ?></span>	
		</div>	
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_21" name="webriti_settings_save_21" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('21');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('21')" >
		</div>		
	</form>
</div>