<?php
/**
* @Theme Name	:	wallstreet-Pro
* @file         :	theme_stup_data.php
* @package      :	wallstreet-Pro
* @author       :	webriti
* @filesource   :	wp-content/themes/wallstreet/theme_stup_data.php
*/
function theme_data_setup()
{	
	$feature_theme_image= WEBRITI_TEMPLATE_DIR_URI . "/images/desk-image.png";
	return $theme_options=array(
			//Logo and Fevicon header			
			'front_page'  => 'on',			
			'upload_image_logo'=>'',
			'height'=>'50',
			'width'=>'250',
			'text_title'=>'on',
			'upload_image_favicon'=>'',
			'webriti_stylesheet'=>'default.css',			
			'google_analytics'=>'',
			'webrit_custom_css'=>'',
			
			//Slide
			'home_slider_enabled'=>'on',
			'animation' => 'slide',								
			'animationSpeed' => '1500',
			'slide_direction' => 'horizontal',
			'slideshowSpeed' => '2500',
			
			// service
			'other_service_section_enabled' => 'on',
			'service_list' => 3,
			'service_title'=> 'Our Services',
			'service_description' => 'We Offer Great Services to our Clients',
			'other_service_title' => 'Other Services',
			'other_service_description' => 'We Offer Great Services to our Clients',
			
			//portfolio
			'view_all_projects_btn_enabled' => 'on',
			'portfolio_list' => 4,
			'portfolio_title' => 'Featured Portfolio Project',
			'portfolio_description' => 'Most Popular of Our Works.',
			'related_portfolio_title' => 'Related Projects',
			'related_portfolio_description' => 'We Offer Great Services to our Clients',
			'portfolio_more_text' => 'View All Projects',
			'portfolio_more_link' => '',			
			'portfolio_more_lnik_target' => 'off',
			
			//Theme Features
			'theme_feature_enabled' => 'on',
			'theme_feature_image' => $feature_theme_image,
			'theme_feature_title' => 'Core Features of Theme',
			'theme_first_feature_icon' => 'fa-sliders',
			'theme_first_title' => 'Incredibly Flexible',
			'theme_first_description' => 'Perspiciatis unde omnis iste natus error sit voluptaem omnis iste.',
			'theme_second_feature_icon' => 'fa-paper-plane-o',
			'theme_second_title' => 'Fully Cutomizable',
			'theme_second_description' => 'Perspiciatis unde omnis iste natus error sit voluptaem omnis iste.',
			'theme_third_feature_icon' => 'fa-bolt',
			'theme_third_title' => 'Ultimate Shortcodes',
			'theme_third_description' => 'Perspiciatis unde omnis iste natus error sit voluptaem omnis iste.',
			
			//client
			'client_list'=>'',
			'total_client'=>'',
			'home_client_title'=>'Our Clients',
			'home_client_description'=>'HAVE A LOOK AT OUR CLIENT WE ARE GROWING THEIR BUSINESS AND THEY ARE GOING UP IN THE MARKET BY BEATING THEIR COMPETITORS',
			
			
			'head_one_team' => 'Meet Our',
			'head_two_team' => 'Great Team',
			'related_project_heading' => 'Related Project',
			
			'call_out_area_enabled' => 'on',
			'call_out_title'=>'You needs help to create a powerful & strong business?',
			'call_out_text'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eros elit, pretium et adipiscing vel, consectetur adipiscing elit.',
			'call_out_button_text'=>'See All Feature',
			'call_out_button_link'=>'#',
			'call_out_button_link_target' =>'on',
			
			// front page
			'front_page_data'=>array('service','portfolio','testimonials','blog','features','client'),
			
			//Testimonial Settings			
			'testi_slide_type' => 'scroll',
			'testi_scroll_items' => '1',
			'testi_timeout_dura' => '2000',
			'testi_scroll_dura' => '1500',
					
			//Post Type slug Options			
			'service_slug' => 'wallstreet_service',
			'portfolio_slug' => 'wallstreet_portfolio',
			
			//contact page settings
			'contact_header_settings' => 'on',			
			'contact_google_map_enabled'=>'on',
			'contact_google_map_title' => 'Location Map',
			'contact_google_map_url' => 'https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Kota+Industrial+Area,+Kota,+Rajasthan&amp;aq=2&amp;oq=kota+&amp;sll=25.003049,76.117499&amp;sspn=0.020225,0.042014&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=Kota+Industrial+Area,+Kota,+Rajasthan&amp;z=13&amp;ll=25.142832,75.879538',
			
			'contact_address_settings' => 'on',
			'contact_address_icon' => 'fa-map-marker',
			'contact_address_title' => 'Address',
			'contact_address_designation_one' => 'Hoffman Parkway, P.O. Box 353',
			'contact_address_designation_two' => 'Mountain View. USA',
			
			'contact_phone_settings' => 'on',
			'contact_phone_icon' => 'fa-phone',
			'contact_phone_title' => 'Phone',
			'contact_phone_number_one' => '1-800-123-789',
			'contact_phone_number_two' => '1-800-123-789',
			
			'contact_email_settings' => 'on',
			'contact_email_icon' => 'fa-inbox',
			'contact_email_title' => 'Email',
			'contact_email_number_one' => 'info@webriti.com',
			'contact_email_number_two' => 'www.webriti.com',
			
			'contact_form_title' => 'Drop Us a Line',
			'contact_form_description' => 'Feel Free to write us a Message',

			// Head Titles
			'about_team_title' => 'Our Great Team',
			'about_team_description' => 'We Offer Great Services to our Clients',
			'home_blog_heading'=>'Our Latest Blog Post',
			'home_blog_description' => 'We Work With New Customers And Grow Their Businesses',
			
			/** Social media links **/
			'about_social_media_enabled'=>'on',
			'header_social_media_enabled'=>'on',			
			'footer_social_media_enabled'=>'on',			
			'social_media_twitter_link' =>"http://twitter.com/",			
			'social_media_facebook_link' =>"http://facebook.com/",
			'social_media_googleplus_link' =>"http://www.google.com",
			'social_media_linkedin_link' =>"http://liknkedin.com/",
			'social_media_pinterest_link' =>"http://pinterest.com/",		
			'social_media_youtube_link' =>"http://youtube.com/",
			'social_media_skype_link' =>"http://skype.com/",			
			'social_media_rssfeed_link' =>"http://rssfeed.com/",		
			'social_media_wordpress_link' =>"http://wordpress.com/",
			'social_media_dropbox_link' =>"http://dropbox.com",
			
			// Typography 
			'enable_custom_typography'=>'off',				
			
			// general typography			
			'general_typography_fontsize'=>'16',
			'general_typography_fontfamily'=>'RobotoRegular',
			'general_typography_fontstyle'=>"",
			
			// menu title
			'menu_title_fontsize'=>'16',
			'menu_title_fontfamily'=>'RobotoRegular',
			'menu_title_fontstyle'=>"",
			
			// post title
			'post_title_fontsize'=>'40',
			'post_title_fontfamily'=>'RobotoLight',
			'post_title_fontstyle'=> "",
					
			// Service  title
			'service_title_fontsize'=>'24',
			'service_title_fontfamily'=>'RobotoRegular',
			'service_title_fontstyle'=>"",
			
			// Potfolio  title Widget Heading Title
			'portfolio_title_fontsize'=>'18',
			'portfolio_title_fontfamily'=>'RobotoMedium',
			'portfolio_title_fontstyle'=>"",
			
			// Widget Heading Title
			'widget_title_fontsize'=>'24',
			'widget_title_fontfamily'=>'RobotoRegular',
			'widget_title_fontstyle'=>"",
			
			// Call out area Title   
			'calloutarea_title_fontsize'=>'24',
			'calloutarea_title_fontfamily'=>'RobotoLight',
			'calloutarea_title_fontstyle'=>"",
			
			// Call out area descritpion      
			'calloutarea_description_fontsize'=>'15',
			'calloutarea_description_fontfamily'=>'RobotoRegular',
			'calloutarea_description_fontstyle'=>"",
			
			// Call out area purches button      
			'calloutarea_purches_fontsize' => '18',	
			'calloutarea_purches_fontfamily' => 'RobotoRegular',	
			'calloutarea_purches_fontstyle' => '',
			
			/** footer customization **/
			'footer_copyright' => 'Copyright @ 2014 - WALL STREET. Designed by <a href="http://webriti.com" rel="nofollow" target="_blank">Webriti</a>',
		);
}
?>