<?php
/**
* @Theme Name	:	wallstreet-Pro
* @file         :	index-client.php
* @package      :	wallstreet-Pro
@author       :	webriti
* @filesource   :	wp-content/themes/wallstreet/index-slider.php
*/
?>
<!-- wallstreet Clients Section ---->
<?php $current_options=get_option('wallstreet_pro_options'); ?>	
<div class="container client-section">	
	<div class="row">			
		<div class="section_heading_title">
			<?php if($current_options['home_client_title']) { ?>
			<h1><?php echo $current_options['home_client_title']; ?></h1>
			<div class="pagetitle-separator"></div>
			<?php } ?>
			<?php if($current_options['home_client_description']) { ?>
				<p><?php echo $current_options['home_client_description']; ?></p>
			<?php } ?>
			
		</div>		
		<div class="row">
			<?php
			$j=1;
			if($current_options['client_list']){
				foreach($current_options['client_list'] as $client_list)
				{ ?>
				<div class="col-md-3">
					<div class="clients-logo">
					<?php if($client_list['client_url'] != '' ) {?>
						<a href="<?php {  echo  $client_list['client_url']; } ?>"><img class="img-responsive" title="<?php if($client_list['client_title'] !='' ) { echo $client_list['client_title']; } ?>" src="<?php echo $client_list['client_image_url']; ?>"></a>
						<?php } else { ?> <img class="img-responsive" title="<?php if($client_list['client_title'] !='' ) { echo $client_list['client_title']; } ?>" src="<?php echo $client_list['client_image_url']; ?>"/><?php } ?>
					</div>
				</div>
				<?php
				if($j%4==0){ echo "<div class='clearfix'></div>"; } $j++;}
			}
			else { 														
				for($tt=1; $tt<=4; $tt++)
				{ ?>
				<div class="col-md-3">
					<div class="clients-logo">
						<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/clients/client<?php echo $tt; ?>.png" class="img-responsive" title="Sonny">
					</div>
				</div>
				<?php 
				}		
			}
			?>			
		</div>		
	</div> 		
</div>
<!-- /wallstreet wallstreet Cliens Section Section ---->