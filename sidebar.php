<?php
/*	@Theme Name	:	Wallstreet-Pro
* 	@file         :	sidebar.php
* 	@package      :	Wallstreet-pro
* 	@filesource   :	wp-content/themes/Wallstreet/sidebar.php
*/	
?>
<!--Sidebar Area-->
		<div class="col-md-4">
			<div class="sidebar-section">
			<?php if ( is_active_sidebar( 'sidebar_primary' ) )
			{ dynamic_sidebar( 'sidebar_primary' );	}
			else  { ?>				
				<!--Categories Widget-->
				<div class="sidebar-widget">
					<div class="sidebar-widget-title"><h2>Categories</h2></div>
					<ul class="post-content">
						<li><a href="#">All</a></li>
						<li><a href="#">Community</a></li>
						<li><a href="#">Graphics</a></li>
						<li><a href="#">Business</a></li>
						<li><a href="#">Corporate</a></li>					
					</ul>
				</div>
				<!--Categories Widget-->
			
				<!--Blog Widget-->
				<div class="sidebar-widget">
					<div class="sidebar-widget-title"><h2>Latest Blog Post</h2></div>
					
					<ul class="sidebar-tab sidebar-widget-tab">
						<li class="active"><a href="#latest" data-toggle="tab">Latest</a></li>
						<li><a href="#recent" data-toggle="tab">Recent</a></li>
						<li><a href="#comment" data-toggle="tab">Comment</a></li>
					</ul>
					
					<div id="myTabContent" class="tab-content">
					
					<div id="latest" class="tab-pane fade active in">
						<div class="row">
							
								<div class="media post-media-sidebar">
									<a class="pull-left sidebar-pull-img" href="#">
										<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/sidebar-img1.jpg" class="img-responsive post_sidebar_img">
									</a>
									<div class="media-body">
										<h3><a href="#">Sus vitae id tortor endisse bulumeu igula lorem.</a></h3>
									</div>
									<div class="sidebar-comment-box">
										<span>April 15, 2014<small>|</small><a href="#">By Admin</a></span>
									</div>
								</div>
								<div class="media post-media-sidebar">
									<a class="pull-left sidebar-pull-img" href="#">
										<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/sidebar-img2.jpg" class="img-responsive post-sidebar-img">
									</a>
									<div class="media-body">
										<h3><a href="#">Sus vitae id tortor endisse bulumeu igula lorem.</a></h3>
									</div>
									<div class="sidebar-comment-box">
										<span>April 15, 2014<small>|</small><a href="#">By Admin</a></span>
									</div>
								</div>
								<div class="media post-media-sidebar">
									<a class="pull-left sidebar-pull-img" href="#">
										<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/sidebar-img3.jpg" class="img-responsive post-sidebar-img">
									</a>
									<div class="media-body">
										<h3><a href="#">Sus vitae id tortor endisse bulumeu igula lorem.</a></h3>
									</div>
									<div class="sidebar-comment-box">
										<span>April 15, 2014<small>|</small><a href="#">By Admin</a></span>
									</div>
								</div>
							
						</div>
					</div>
					
					<div id="recent" class="tab-pane fade">
						<div class="row">
							
								<div class="media post-media-sidebar">
									<a class="pull-left sidebar-pull-img" href="#">
										<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/sidebar-img1.jpg" class="img-responsive post-sidebar-img">
									</a>
									<div class="media-body">
										<h3><a href="#">Sus vitae id tortor endisse bulumeu igula lorem.</a></h3>
									</div>
									<div class="sidebar-comment-box">
										<span>April 15, 2014<small>|</small><a href="#">By Admin</a></span>
									</div>
								</div>
								<div class="media post-media-sidebar">
									<a class="pull-left sidebar-pull-img" href="#">
										<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/sidebar-img2.jpg" class="img-responsive post-sidebar-img">
									</a>
									<div class="media-body">
										<h3><a href="#">Sus vitae id tortor endisse bulumeu igula lorem.</a></h3>
									</div>
									<div class="sidebar-comment-box">
										<span>April 15, 2014<small>|</small><a href="#">By Admin</a></span>
									</div>
								</div>
							
						</div>
					</div>
					
					<div id="comment" class="tab-pane fade">
						<div class="row">
							
								<div class="media post-media-sidebar">
									<a class="pull-left sidebar-pull-img" href="#">
										<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/sidebar-img1.jpg" class="img-responsive post-sidebar-img">
									</a>
									<div class="media-body">
										<h3><a href="#">Sus vitae id tortor endisse bulumeu igula lorem.</a></h3>
									</div>
									<div class="sidebar-comment-box">
										<span>April 15, 2014<small>|</small><a href="#">By Admin</a></span>
									</div>
								</div>								
							
						</div>
					</div>
					
				</div>
					
					
				</div>
				<!--/Blog Widget-->
					
				<!--Tags Widget-->
				<div class="sidebar-widget">
					<div class="sidebar-widget-title"><h2>Tags Cloud</h2></div>
					<div class="sidebar-widget-tags">
						<a href="#">Wordpress</a>
						<a href="#">business</a>
						<a href="#">Jquery</a>
						<a href="#">Css</a>
						<a href="#">Php</a>
						<a href="#">photoshop</a>
						<a href="#">Css</a>	
						<a href="#">template</a>
						<a href="#">Jquery</a>
						<a href="#">Css</a>
						<a href="#">Php</a>
						<a href="#">photoshop</a>						
					</div>
				</div>
				<?php } ?>
				<!--Tags Widget-->
			</div>
		</div>
		<!--Sidebar Area-->