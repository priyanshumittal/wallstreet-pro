jQuery(function() {
			//	This js For Homepage Testimonial Section
			jQuery('#testimonial-scroll').carouFredSel({
				//width: '100%',
				//height : "auto",
				responsive : true,
				circular: true,
				auto : true,
				items: {
						height : "variable",
                        visible: {
                            min: 1,
                            max: 2
                        }
                    },
				prev: '#prev3',
				next: '#next3',
				directon: 'left',
				auto: true,
				 scroll : {
						items : 1,
						duration : 2000,
						//fx:"crossfade",
						//easing: "elastic",
						timeoutDuration : 1500
					},
			}).trigger('resize');
			
			

			
			
			
					
			
			
			
			
			//	This js For Homepage Tweeter Section
			jQuery('#tweet-scroll').carouFredSel({
				//width: '100%',
				height : "variable",
				responsive : true,
				circular: true,
				items: {
						height : "variable",
                        visible: {
                            min: 1,
                            max: 2,
                        }
                    },
				directon: 'left',
				auto: true,
				 scroll : {
						items : 1,
						duration : 2000,
						fx:"fade",
						//easing: "elastic",
						timeoutDuration : 1500
					},

			}).trigger('resize');
			
			//	This js For Portfolio Related Projects Section
			jQuery('#related_project_scroll').carouFredSel({
				width: '100%',
				responsive : true,
				circular: true,
				items: {
						height : '85%',
                        visible: {
                            min: 1,
                            max: 3,
                        }
                    },
				directon: 'left',
				auto: true,
				prev: '#project_prev',
				next: '#project_next',
				 scroll : {
						items : 1,
						duration : 1200,
						//fx:"fade",
						//easing: "elastic",
						timeoutDuration : 2000
					},
			});
			
			
			
			
			
			
			jQuery('#scroll').carouFredSel({
				//width: '100%',
				height : "auto",
				responsive : true,
				circular: true,
				pagination: "#pager2",
				items: {
                        visible: {
                            min: 1,
                            max: 2
                        }
                    },
				prev: '#prev3',
				next: '#next3',
				directon: 'left',

				auto: true,
				 scroll : {
						items : 1,
						duration : 1500,
						//fx:"uncover-fade",
						//easing: "elastic",
						timeoutDuration : 1500
					},

			});
			
		});